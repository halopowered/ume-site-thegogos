<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'thegogos_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9n`gMp#~ps!ttl>OUxcJV9UscKK7)_A> 9v(G>1 dp>g)g]Dwq?e%CjIk_=NG2s>' );
define( 'SECURE_AUTH_KEY',  'fGl<bFw%#%=(U9f$ktF 0_rS5Be9|pubb0^R5=*~36lMs>1uIeA.,d*lD*D&2s$6' );
define( 'LOGGED_IN_KEY',    'C&]ZB3uhi{BR8.,aCR8CJ0#25X;Z^dTA^<o6 uiCr<GEAA&c0tt7K7Sa&Sd}Mo=C' );
define( 'NONCE_KEY',        'X80*uf|<|Ztu?3w;9Hw=l7DaKz{wiZO,$fuJ?Z8&aCfGLola2einK.4]/.)E#t^[' );
define( 'AUTH_SALT',        'd/A_gycDD;hRO&Za>-8s.Z,*!QCEnHhcaz{3|!ie?ueXH8CMhUK)=bSI9}evYCf[' );
define( 'SECURE_AUTH_SALT', '2mn/5!|Sp)tx;RU!dD]^>_lB.OSfR{d:n&[iFXBo3k.F&:NzW/nbA&U->>q _eCl' );
define( 'LOGGED_IN_SALT',   'q)y@B7Lz![RJ`l>hkiRcUAdNqkjiHwBxBpWea-(MT=I{~ReiWu7k? t$N2U3Zt=$' );
define( 'NONCE_SALT',       '{O7baD*qhshO7PgOHvmNc.NXF_Oycg.VXHp<7SbdR:mQQW[lBgo54$p7^i>C6gma' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
