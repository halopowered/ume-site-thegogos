<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php
	$get_author_id = get_the_author_meta('ID');
    $get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));
	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
			$dir = get_template_directory_uri().'/assets/';
			// get_template_part( 'template-parts/content', get_post_type() );
			?>

			<div class="news-detail-container">
				<div class="news-image">
					<?php if ( has_post_thumbnail() ): ?>
						<?php the_post_thumbnail(); ?>
					<?php else: ?>
						<img src="<?php echo $dir; ?>images/news-2.png">
					<?php endif; ?>
				</div>
				<div class="news-category">
					<p><?php echo get_the_category( $id )[0]->name; ?></p>
				</div>
				<div class="field--name-title">
					<h2><?php the_title(); ?></h2>
				</div>
				<div class="block-container">
					<div class="news-author-image">
						<?php echo '<img src="'.$get_author_gravatar.'" />';?>
					</div>
					<div class="block-wrapper">
						<div class="field--name-author">
							<p>By <?php the_author(); ?></p>
						</div>
						<div class="field--name-post-date">
							<p><?php echo get_the_date(); ?></p>
						</div>
					</div>
				</div>

				<div class="field--name-body">
					<p><?php the_content(); ?></p>
				</div>
			</div>

			<?php
		
		}
	}

	?>

</main><!-- #site-content -->

<?php get_footer(); ?>
