<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content" role="main">
	<?php 
		$musicList = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 12, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged ) );
	?>
	<?php if ( have_posts() ) { ?>
		<section class="l-banner">
			<?php
			while ( have_posts() ) {
				the_post();
				$provider_link = get_field('provider_link'); 
				if( $provider_link ) {
					$link_url = $provider_link['url'];
					$link_title = $provider_link['title'];
					$link_target = $provider_link['target'] ? $provider_link['target'] : '_self';
					if ($link_title) {
						$link_T = $provider_link['title'];
					} else {
						$link_T = 'Stream/Download';
					}
				} 
			?>
				<article class="node--music-banner">
					<section class="l-front">
						<div class="l-column-wrapper">
							<div class="ds-region--column-1">
								<div class="field--name-field-headline-image">
									<?php the_post_thumbnail(); ?>
								</div>
							</div>
							<div class="ds-region--column-2">
								<div class="field--name-title">
									<h2><?php the_title(); ?></h2>
								</div>
								<div class="field--name-post-date">
									<p><?php the_date(); ?></p>
								</div>
								<div class="field--name-providers">
									<a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_T; ?></a>
								</div>
							</div>
						</div>
					</section>
				</article>
			<?php } ?>
			</section>
	<?php } ?>

	<?php  if( $musicList->have_posts() ) : ?>
		<div class="more-nodes-container">
			<div class="view-content container">
				<?php 
					while ( $musicList->have_posts() ) : $musicList->the_post(); 

					$provider_link = get_field('provider_link'); 
					if( $provider_link ) {
						$link_url = $provider_link['url'];
						$link_title = $provider_link['title'];
						$link_target = $provider_link['target'] ? $provider_link['target'] : '_self';
						if ($link_title) {
							$link_T = $provider_link['title'];
						} else {
							$link_T = 'Stream/Download';
						}
					} 
				?>

					<article class="node--music">
						<div class="field--name-field-headline-image">
							<?php the_post_thumbnail(); ?>
							<div class="node-overlay">
								<div class="field--name-providers">
									<a href="<?php the_permalink(); ?>">Read More</a>
								</div>
							</div>
						</div>
						<div class="videos-footer">
							<div class="field--name-title">
								<h2><?php the_title(); ?></h2>
							</div>
							<div class="field--name-post-date">
								<p><?php echo get_the_date(); ?></p>
							</div>
						</div>
					</article>

				<?php endwhile; ?>
			
			</div>

			<?php if (function_exists("pagination")) { pagination($musicList->max_num_pages); } ?>
		</div>
	<?php endif; ?>


</main><!-- #site-content -->

<?php get_footer(); ?>
