<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>
	<?php 
		$dir = get_template_directory_uri().'/assets/';
		$dist = get_template_directory_uri().'/dist/';
		$fb = get_field('facebook', 'option'); 
		$ig = get_field('instagram', 'option'); 
		$sc = get_field('soundcloud', 'option'); 
		$spot = get_field('spotify', 'option'); 
		$apple = get_field('apple', 'option'); 
		$twitter = get_field('twitter', 'option'); 
		$yt = get_field('youtube', 'option'); 
	?>
	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" crossorigin="anonymous">
		<script src=“https://kit.fontawesome.com/232cdc2521.js” crossorigin=“anonymous”></script>
		<!-- <script src="http://localhost:8080/dist/bundle.js"></script> -->
		<link rel="stylesheet" href="<?php echo $dist; ?>style.css"> 
		<script src="<?php echo $dist; ?>bundle.js"></script>
		
	</head>


	<body <?php body_class(); ?>>
	<div class="mobile-menu">
		<div class="mobile-main-nav">
			<?php
			if ( has_nav_menu( 'primary' ) ) {
				?>

					<nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'Horizontal', 'twentytwenty' ); ?>" role="navigation">

						<ul class="primary-menu reset-list-style">

						<?php
						if ( has_nav_menu( 'primary' ) ) {

							wp_nav_menu(
								array(
									'container'  => '',
									'items_wrap' => '%3$s',
									'theme_location' => 'primary',
								)
							);

						}
						?>

						</ul>

					</nav><!-- .primary-menu-wrapper -->

				<?php
			}
			?>
		</div>
		<div class="mobile-main-social">
			<ul>
				<?php if($fb): ?>
				<li>
					<a href="<?php echo $fb; ?>" target="_blank">
						<i class="fab fa-facebook-f"></i>
					</a>
				</li>
				<?php endif; if($ig):?>
				<li>
					<a href="<?php echo $ig; ?>" target="_blank">
						<i class="fab fa-instagram"></i>
					</a>
				</li>
				<?php endif; if($sc):?>
				<li>
					<a href="<?php echo $sc; ?>" target="_blank">
						<i class="fab fa-soundcloud"></i>
					</a>
				</li>
				<?php endif; if($spot):?>
				<li>
					<a href="<?php echo $spot; ?>" target="_blank">
						<i class="fab fa-spotify"></i>
					</a>
				</li>
				<?php endif; if($apple):?>
				<li>
					<a href="<?php echo $apple; ?>" target="_blank">
						<i class="fab fa-apple"></i>
					</a>
				</li>
				<?php endif; if($twitter):?>
				<li>
					<a href="<?php echo $twitter; ?>" target="_blank">
						<i class="fab fa-twitter"></i>
					</a>
				</li>
				<?php endif; if($yt):?>
				<li>
					<a href="<?php echo $yt; ?>" target="_blank">
						<i class="fab fa-youtube"></i>
					</a>
				</li>
				<?php endif;?>
			</ul>
		</div>
	</div>
	<main class="l-main">
		<a href="#" class="exit-off-canvas"></a>
		<header class="main-header">
			<div class="header-container">
				<div class="menu-icon">
					<a href="#"><span></span><span></span><span></span></a>
				</div>
				<div class="l-branding">
					<h2 class="site-name">
						<a href="/" title="The Go-Go's"><img src="<?php echo $dir; ?>images/site-logo.png" alt="The Go-Go's"></a>
					</h2>
				</div>
				<div class="l-navigation">
					<div class="header-main-nav">

					<?php
					if ( has_nav_menu( 'primary' ) ) {
						?>

							<nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'Horizontal', 'twentytwenty' ); ?>" role="navigation">

								<ul class="primary-menu reset-list-style">

								<?php
								if ( has_nav_menu( 'primary' ) ) {

									wp_nav_menu(
										array(
											'container'  => '',
											'items_wrap' => '%3$s',
											'theme_location' => 'primary',
										)
									);

								}
								?>

								</ul>

							</nav><!-- .primary-menu-wrapper -->

						<?php
					}
					?>
					</div>
					<div class="header-main-social">
						<ul>
							<?php if($fb): ?>
							<li>
								<a href="<?php echo $fb; ?>" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<?php endif; if($ig):?>
							<li>
								<a href="<?php echo $ig; ?>" target="_blank">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
							<?php endif; if($sc):?>
							<li>
								<a href="<?php echo $sc; ?>" target="_blank">
									<i class="fab fa-soundcloud"></i>
								</a>
							</li>
							<?php endif; if($spot):?>
							<li>
								<a href="<?php echo $spot; ?>" target="_blank">
									<i class="fab fa-spotify"></i>
								</a>
							</li>
							<?php endif; if($apple):?>
							<li>
								<a href="<?php echo $apple; ?>" target="_blank">
									<i class="fab fa-apple"></i>
								</a>
							</li>
							<?php endif; if($twitter):?>
							<li>
								<a href="<?php echo $twitter; ?>" target="_blank">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<?php endif; if($yt):?>
							<li>
								<a href="<?php echo $yt; ?>" target="_blank">
									<i class="fab fa-youtube"></i>
								</a>
							</li>
							<?php endif;?>
						</ul>
					</div>
				</div>
			</div>	
		</header>