<?php
    /**
     * Template Name: Tour Template
     */
    get_header();
    $dir = get_template_directory_uri().'/assets/';
    $posts = get_posts( array(
        'post_type' => 'umg_live',
        'posts_per_page' => -1,
        'meta_key' => 'gigdate',
        'orderby' => 'meta_value',
        'order' => 'ASC'
    ));
?>

<h2 class="block__title">Upcoming Dates</h2>
    <div class="block__content">
        <div class="view">
            <div class="view-content container">
                <?php if( $posts ): ?>
                    <?php foreach( $posts as $post ): ?>
                        <?php 
                            $gdate = date_create(''.get_field('gigdate').'');
                            $num = date_format($gdate,'d');
                            $date = date_format($gdate,'M');
                            $year = date_format($gdate,'Y');
                            $dayOfWeek = date_format($gdate,'D');
                            $venue = get_field( 'gigvenue' );
                            $city = get_field( 'gigvenuecity' );
                            $tickets = get_field( 'gigticketlink' ); 
                        ?>
                        <article class="node--event">
                            <div class="event-container">
                                <div class="left">
                                    <div class="tour-date">
                                        <span class="days"><?php echo $dayOfWeek ?></span>
                                        <h2><span class="month"><?php echo $date ?></span><span class="date"><?php echo $num ?></span></h2>
                                        <span class="year"><?php echo $year ?></span>
                                    </div>
                                </div>
                                <div class="middle">
                                    <div class="tour-info">
                                        <div class="tour-name">
                                            <?php echo $venue; ?>
                                        </div>
                                        <div class="tour-location">
                                            <?php echo $city; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="tour-tickets">
                                        <?php if ($tickets): ?>
                                            <a href="<?php echo $tickets; ?>" target="_blank">Get Tickets</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php endforeach; ?>
                <?php elseif ( !empty( get_the_content() ) ): ?>
                    <div style="text-align: center; color: #fff; font-size: 22px;">
                        <?php the_content(); ?>
                    </div>
                <?php endif; wp_reset_query(); ?>
            </div>
        </div>
    </div>


<?php get_footer(); ?>