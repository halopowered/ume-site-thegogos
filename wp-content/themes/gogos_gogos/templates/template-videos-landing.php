<?php
    /**
     * Template Name: Videos Landing Template
     */
    get_header();
    $dir = get_template_directory_uri().'/assets/';
    $videoBanner = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
    $videoList = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 3, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged ) );
?>

<?php  if( $videoBanner->have_posts() ) : ?>
    <section class="l-banner">
        <?php 
            while ( $videoBanner->have_posts() ) : $videoBanner->the_post(); 
            $youtube_id = get_field('youtube_id'); 
        ?>
            <article class="node--video-banner">
                <section class="l-front">
                    <div class="l-column-wrapper">
                        <div class="ds-region--column-1">
                            <div class="field--name-field-headline-image">
                                <?php if ( has_post_thumbnail() ): ?>
                                    <?php the_post_thumbnail(); ?>
                                <?php else: ?>
                                    <img src="https://img.youtube.com/vi/<?php echo $youtube_id ?>/maxresdefault.jpg">
                                <?php endif; ?>
                            </div>
                            <div class="video-foreground">
                                <div class="field--name-node-link">
                                    <a href="#">Play</a>
                                </div>
                                <div class="field--name-field-youtube-id">
                                    <p><?php echo $youtube_id ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="ds-region--column-2">
                            <div class="field--name-title">
                                <h2><?php the_title(); ?></h2>
                            </div>
                            <div class="field--name-post-date">
                                <p><?php the_date(); ?></p>
                            </div>
                            <div class="field--name-body">
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
        <?php endwhile; ?>
    </section>
<?php endif; wp_reset_query(); ?>

<?php  if( $videoList->have_posts() ) : ?>
    <div class="more-nodes-container">
        <div class="view-content container">
            <?php 
                while ( $videoList->have_posts() ) : $videoList->the_post(); 
                $youtube_id = get_field('youtube_id'); 
            ?>
                <article class="node--videos">
                    <div class="field--name-field-headline-image">
                        <?php if ( has_post_thumbnail() ): ?>
                            <?php the_post_thumbnail(); ?>
                        <?php else: ?>
                            <img src="https://img.youtube.com/vi/<?php echo $youtube_id ?>/maxresdefault.jpg">
                        <?php endif; ?>
                        <div class="node-overlay">
                            <a href="https://www.youtube.com/watch?v=<?php echo $youtube_id ?>" class="iframe">Watch</a>
                        </div>
                    </div>
                    <div class="videos-footer">
                        <div class="field--name-title">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <div class="field--name-post-date">
                            <p><?php echo get_the_date(); ?></p>
                        </div>
                        <div class="field--name-body">
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                </article>
            <?php endwhile; ?>

        </div>
        <?php if (function_exists("pagination")) { pagination($videoList->max_num_pages); } ?>
    </div>
<?php endif; wp_reset_query(); ?>


<?php get_footer(); ?>