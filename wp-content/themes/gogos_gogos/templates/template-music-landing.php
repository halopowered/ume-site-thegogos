<?php
    /**
     * Template Name: Music Landing Template
     */
    get_header();
    $dir = get_template_directory_uri().'/assets/';
    $musicBanner = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
    $musicList = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 12, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged ) );
?>

<?php  if( $musicBanner->have_posts() ) : ?>
    <section class="l-banner">
        <?php 
            while ( $musicBanner->have_posts() ) : $musicBanner->the_post(); 

            $provider_link = get_field('provider_link'); 
            if( $provider_link ) {
                $link_url = $provider_link['url'];
                $link_title = $provider_link['title'];
                $link_target = $provider_link['target'] ? $provider_link['target'] : '_self';
                if ($link_title) {
                    $link_T = $provider_link['title'];
                } else {
                    $link_T = 'Stream/Download';
                }
            } 
        ?>
            <article class="node--music-banner">
                <section class="l-front">
                    <div class="l-column-wrapper">
                        <div class="ds-region--column-1">
                            <div class="field--name-field-headline-image">
                                <?php the_post_thumbnail(); ?>
                            </div>
                        </div>
                        <div class="ds-region--column-2">
                            <div class="field--name-title">
                                <h2><?php the_title(); ?></h2>
                            </div>
                            <div class="field--name-post-date">
                                <p><?php the_date(); ?></p>
                            </div>
                            <?php if( $provider_link ) : ?>
                                <div class="field--name-providers">
                                    <a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_T; ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
            </article>
        <?php endwhile; ?>
    </section>
<?php endif; wp_reset_query(); ?>

<?php  if( $musicList->have_posts() ) : ?>
    <div class="more-nodes-container">
        <div class="view-content container">
            <?php 
                while ( $musicList->have_posts() ) : $musicList->the_post(); 

                $provider_link = get_field('provider_link'); 
                if( $provider_link ) {
                    $link_url = $provider_link['url'];
                    $link_title = $provider_link['title'];
                    $link_target = $provider_link['target'] ? $provider_link['target'] : '_self';
                    if ($link_title) {
                        $link_T = $provider_link['title'];
                    } else {
                        $link_T = 'Stream/Download';
                    }
                } 
            ?>

                <article class="node--music">
                    <div class="field--name-field-headline-image">
                        <?php the_post_thumbnail(); ?>
                        <?php if( $provider_link ) : ?>
                            <div class="node-overlay">
                                <div class="field--name-providers">
                                    <a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_T; ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="videos-footer">
                        <div class="field--name-title">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <div class="field--name-post-date">
                            <p><?php echo get_the_date(); ?></p>
                        </div>
                    </div>
                </article>

            <?php endwhile; ?>
        
        </div>

        <?php if (function_exists("pagination")) { pagination($musicList->max_num_pages); } ?>
    </div>
<?php endif; ?>


<?php get_footer(); ?>