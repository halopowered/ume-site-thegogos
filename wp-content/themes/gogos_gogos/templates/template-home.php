<?php
    /**
     * Template Name: Home Template
     */
    get_header();
    while ( have_posts() ) : the_post();
    $dir = get_template_directory_uri().'/assets/';
    $music = new WP_Query( array('post_type' => 'release', 'posts_per_page' => 4, 'orderby' => 'date', 'order' => 'DESC' ) );
    $video = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
    $news = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 3, 'orderby' => 'date', 'order' => 'DESC' ) );
    $events = new WP_Query( array(
        'post_type' => 'umg_live',
        'posts_per_page' => 5,
        'meta_key' => 'gigdate',
        'orderby' => 'meta_value',
        'order' => 'ASC'
    ));
?>

    <!-- banner start -->
    <?php if( have_rows('banner') ): ?>
    <section class="l-banner">
        <div class="l-region--banner">
            <?php 
                while( have_rows('banner') ) : the_row();
                $image = get_sub_field('headline_image');
                $cta = get_sub_field('cta');
                if( $cta ) {
                    $link_url = $cta['url'];
                    $link_title = $cta['title'];
                    $link_target = $cta['target'] ? $cta['target'] : '_self';
                } 
            ?>
                <article class="node--hero">
                    <section class="l-front">
                        <div class="ds-region--background">
                            <div class="field--name-field-headline-image">
                                <img src="<?php echo $image ?>">
                            </div>
                        </div>
                        <div class="ds-region--foreground">
                            <div class="field--name-body">
                                <div class="hero-contents">
                                    <?php if ( $cta) { ?>
                                        <div class="cta">
                                            <a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ?></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
            <?php endwhile;?>
        </div>
    </section>
    <?php  endif; wp_reset_query();?>
    <!-- banner end -->

    <?php  if( $music->have_posts() ) : ?>
        <div class="release-block">
            <div class="block__content">
                <div class="view">
                    <div class="view-content container">
                        <?php 
                            while ( $music->have_posts() ) : $music->the_post(); 

                            $provider_link = get_field('provider_link'); 
                            if( $provider_link ) {
                                $link_url = $provider_link['url'];
                                $link_title = $provider_link['title'];
                                $link_target = $provider_link['target'] ? $provider_link['target'] : '_self';
                                if ($link_title) {
                                    $link_T = $provider_link['title'];
                                } else {
                                    $link_T = 'Stream/Download';
                                }
                            } 
                        ?>
                            <article class="node--release">
                                <div class="l-article-main">
                                    <div class="l-column-wrapper">
                                        <div class="ds-region--column-1">
                                            <div class="field--name-field-release-cover">
                                                <?php the_post_thumbnail(); ?>
                                            </div>
                                        </div>
                                        <div class="ds-region--column-2">
                                            <div class="field--name-artist">
                                                <p>The Go-Go's</p>
                                            </div>
                                            <div class="field--name-title">
                                                <h2><?php the_title(); ?></h2>
                                            </div>
                                            <?php if( $provider_link ) : ?>
                                                <div class="field--name-providers">
                                                    <a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_T; ?></a>
                                                </div>
                                            <?php  endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php  endif; wp_reset_query();?>

    <?php  if( $events->have_posts() ) : ?>
        <div class="tour-block">
            <h2 class="block__title">Upcoming Dates</h2>
            <div class="block__content">
                <div class="view">
                    <div class="view-content container">
                        <?php 
                            while ( $events->have_posts() ) : $events->the_post(); 

                            $gdate = date_create(''.get_field('gigdate').'');
                            $num = date_format($gdate,'d');
                            $date = date_format($gdate,'M');
                            $year = date_format($gdate,'Y');
                            $dayOfWeek = date_format($gdate,'D');
                            $venue = get_field( 'gigvenue' );
                            $city = get_field( 'gigvenuecity' );
                            $tickets = get_field( 'gigticketlink' ); 
                        ?>
                            <article class="node--event">
                                <div class="event-container">
                                    <div class="left">
                                        <div class="tour-date">
                                            <span class="days"><?php echo $dayOfWeek ?></span>
                                            <h2><span class="month"><?php echo $date ?></span><span class="date"><?php echo $num ?></span></h2>
                                            <span class="year"><?php echo $year ?></span>
                                        </div>
                                    </div>
                                    <div class="middle">
                                        <div class="tour-info">
                                            <div class="tour-name">
                                                <?php echo $venue; ?>
                                            </div>
                                            <div class="tour-location">
                                                <?php echo $city; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="tour-tickets">
                                            <?php if ($tickets): ?>
                                                <a href="<?php echo $tickets; ?>" target="_blank">Get Tickets</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <div class="more-link">
                <a href="/tour">More Dates</a>
            </div>
        </div>
    <?php elseif ( !empty( get_post(38) ) ): ?>
        <div class="tour-block">
            <h2 class="block__title">Upcoming Dates</h2>
            <div class="block__content">
                <div class="view">
                    <div class="view-content container">
                        <div style="text-align: center; color: #fff; font-size: 22px;">
                            <?php
                                $post   = get_post( 38 );

                                $output =  apply_filters( 'the_content', $post->post_content );

                                echo $output;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php  endif; wp_reset_query();?>

    <?php  if( $video->have_posts() ) : ?>
        <div class="videos-block">
            <div class="block__content">
                <div class="view">
                    <div class="view-content container">
                        <?php 
                            while ( $video->have_posts() ) : $video->the_post(); 
                            $youtube_id = get_field('youtube_id'); 
                        ?>
                            <article class="node--video">
                                <section class="l-front">
                                    <div class="ds-region--background">
                                        <div class="field--name-field-headline-image">
                                            <?php if ( has_post_thumbnail() ): ?>
                                                <?php the_post_thumbnail(); ?>
                                            <?php else: ?>
                                                <img src="https://img.youtube.com/vi/<?php echo $youtube_id ?>/maxresdefault.jpg">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="ds-region--foreground">
                                        <div class="field--name-artist">
                                            <p>The Go-Go's</p>
                                        </div>
                                        <div class="field--name-title">
                                            <h2><?php the_title(); ?></h2>
                                        </div>
                                        <div class="field--name-node-link">
                                            <a href="#">Play</a>
                                        </div>
                                        <div class="field--name-field-youtube-id">
                                            <p><?php echo $youtube_id ?></p>
                                        </div>
                                    </div>
                                </section>
                            </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; wp_reset_query();?>

    <?php  if( $news->have_posts() ) : ?>
    <div class="news-block">
        <div class="block__content">
            <div class="view">
                <div class="view-content container">
                    <?php 
                        while ( $news->have_posts() ) : $news->the_post(); 
                    ?>
                        <article class="node--news">
                            <div class="news-image">
                                <?php if ( has_post_thumbnail() ): ?>
                                    <?php the_post_thumbnail(); ?>
                                <?php else: ?>
                                    <img src="<?php echo $dir; ?>images/news-2.png">
                                <?php endif; ?>
                                <div class="news-category">
                                    <p><?php echo get_the_category( $id )[0]->name; ?></p>
                                </div>
                                <div class="node-overlay">
                                    <a href="<?php the_permalink(); ?>">Read More</a>
                                </div>
                            </div>
                            <div class="news-footer">
                                <div class="field--name-title">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                                <div class="field--name-body">
                                    <p><?php the_content(); ?></p>
                                </div>
                                <div class="field--name-author">
                                    <p>By <?php the_author(); ?></p>
                                </div>
                                <div class="field--name-post-date">
                                    <p><?php echo get_the_date(); ?></p>
                                </div>
                            </div>
                        </article>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <div class="more-link">
            <a href="/news">More News</a>
        </div>
    </div>
    <?php endif; wp_reset_query();?>

    <?php endwhile; ?>
<?php get_footer(); ?>