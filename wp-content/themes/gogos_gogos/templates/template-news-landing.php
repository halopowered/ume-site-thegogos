<?php
    /**
     * Template Name: News Landing Template
     */
    get_header();
    $dir = get_template_directory_uri().'/assets/';
    $newsBanner = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
    $newsList = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 12, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged ) );
    $get_author_id = get_the_author_meta('ID');
    $get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450));
?>
<?php  if( $newsBanner->have_posts() ) : ?>
    <section class="l-banner">
        <?php 
            while ( $newsBanner->have_posts() ) : $newsBanner->the_post(); 
        ?>
            <article class="node--news-banner">
                <div class="l-front">
                    <div class="l-column-wrapper">
                        <div class="ds-region--column-1">
                            <div class="news-image">
                                <?php if ( has_post_thumbnail() ): ?>
                                    <?php the_post_thumbnail(); ?>
                                <?php else: ?>
                                    <img src="<?php echo $dir; ?>images/news-2.png">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="ds-region--column-2">
                            <div class="news-category">
                                <p><?php echo get_the_category( $id )[0]->name; ?></p>
                            </div>
                            <div class="field--name-title">
                                <h2><?php the_title(); ?></h2>
                            </div>
                            <div class="field--name-body">
                                <p><?php the_content(); ?></p>
                            </div>
                            <!-- <div class="block-container">
                                <div class="news-author-image">
                                    <?php /*echo '<img src="'.$get_author_gravatar.'" />'; */ ?>
                                </div>
                                <div class="block-wrapper">
                                    <div class="field--name-author">
                                        <p>By <?php /* the_author(); */?></p>
                                    </div>
                                    <div class="field--name-post-date">
                                        <p><?php /* the_date(); */?></p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>  
                </div>
            </article>
        <?php endwhile; ?>
    </section>
<?php endif; wp_reset_query(); ?>

<?php  if( $newsList->have_posts() ) : ?>
    <div class="more-nodes-container">
        <div class="view-content container">
            <?php 
                while ( $newsList->have_posts() ) : $newsList->the_post(); 
            ?>
                <article class="node--news">
                    <div class="news-image">
                        <?php if ( has_post_thumbnail() ): ?>
                            <?php the_post_thumbnail(); ?>
                        <?php else: ?>
                            <img src="<?php echo $dir; ?>images/news-2.png">
                        <?php endif; ?>
                        <div class="news-category">
                            <p><?php echo get_the_category( $id )[0]->name; ?></p>
                        </div>
                        <div class="node-overlay">
                            <a href="<?php the_permalink(); ?>">Read More</a>
                        </div>
                    </div>
                    <div class="news-footer">
                        <div class="field--name-title">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <div class="field--name-body">
                            <p><?php the_content(); ?></p>
                        </div>
                        <!-- <div class="field--name-author">
                            <p>By <?php /* the_author(); */ ?></p>
                        </div>
                        <div class="field--name-post-date">
                            <p><?php /* echo get_the_date(); */ ?></p>
                        </div> -->
                    </div>
                </article>
            <?php endwhile; ?>
        </div>
        <?php if (function_exists("pagination")) { pagination($newsList->max_num_pages); } ?>
    </div>
<?php endif; ?>


<?php get_footer(); ?>