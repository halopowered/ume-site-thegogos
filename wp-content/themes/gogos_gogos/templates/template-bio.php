<?php
    /**
     * Template Name: Bio Template
     */
    get_header();
    $dir = get_template_directory_uri().'/assets/';
    // $bioBanner = new WP_Query( array('post_type' => 'video', 'posts_per_page' => 1, 'orderby' => 'date', 'order' => 'DESC' ) );
    // $bioBanner = get_the_post_thumbnail_url( int|WP_Post $post = null, string|array $size = 'post-thumbnail' )
    // $post_id = 30;
    $post = get_post();
    $blocks = parse_blocks($post->post_content);

?>

<section class="l-banner">
    <div class="l-region--banner">
        <article class="node--bio-banner">
            <section class="l-front">
                <div class="ds-region--background">
                    <div class="field--name-field-headline-image">
                        <img src="<?php echo get_the_post_thumbnail(); ?>">
                    </div>
                </div>
            </section>
        </article>
    </div>
</section>

<div class="body-container bio-main">
    <div class="inner-wrapper">
        <?php 
            if ($blocks[0]['blockName'] === 'core/html') {
                echo render_block($blocks[0]);
            }
        ?>
    </div>
</div>
<div class="bio-photos-container">
    <div class="photos-carousel">
        <?php 
            if ($blocks[2]['blockName'] === 'core/gallery') {
                echo render_block($blocks[2]);
            }
        ?>
    </div>
</div>

<div class="body-container">
    <div class="inner-wrapper">
        <?php 
            if ($blocks[4]['blockName'] === 'core/html') {
                echo render_block($blocks[4]);
            }
        ?>
    </div>
</div>


<?php get_footer(); ?>