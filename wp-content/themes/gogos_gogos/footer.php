<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
	</main>
	<footer class="main-footer">
		<div class="l-region--footer">

		</div>
		<nav>
			<div class="l-region--footer-nav">
				<div class="block-footer-legal-nav">
					<?php
					if ( has_nav_menu( 'footer' ) ) {
						?>

							<nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'footer', 'twentytwenty' ); ?>" role="navigation">

								<ul class="primary-menu reset-list-style">

								<?php
								if ( has_nav_menu( 'footer' ) ) {

									wp_nav_menu(
										array(
											'container'  => '',
											'items_wrap' => '%3$s',
											'theme_location' => 'footer',
										)
									);

								}
								?>

								</ul>

							</nav><!-- .primary-menu-wrapper -->

						<?php
					}
					?>
				</div>
				<div class="block-footer-socials">
				<?php 
					$dir = get_template_directory_uri().'/assets/';
					$fb = get_field('facebook', 'option'); 
					$ig = get_field('instagram', 'option'); 
					$sc = get_field('soundcloud', 'option'); 
					$spot = get_field('spotify', 'option'); 
					$apple = get_field('apple', 'option'); 
					$twitter = get_field('twitter', 'option'); 
					$yt = get_field('youtube', 'option'); 
				?>
					<ul>
						<?php if($fb): ?>
						<li>
							<a href="<?php echo $fb; ?>" target="_blank">
								<i class="fab fa-facebook-f"></i>
							</a>
						</li>
						<?php endif; if($ig):?>
						<li>
							<a href="<?php echo $ig; ?>" target="_blank">
								<i class="fab fa-instagram"></i>
							</a>
						</li>
						<?php endif; if($sc):?>
						<li>
							<a href="<?php echo $sc; ?>" target="_blank">
								<i class="fab fa-soundcloud"></i>
							</a>
						</li>
						<?php endif; if($spot):?>
						<li>
							<a href="<?php echo $spot; ?>" target="_blank">
								<i class="fab fa-spotify"></i>
							</a>
						</li>
						<?php endif; if($apple):?>
						<li>
							<a href="<?php echo $apple; ?>" target="_blank">
								<i class="fab fa-apple"></i>
							</a>
						</li>
						<?php endif; if($twitter):?>
						<li>
							<a href="<?php echo $twitter; ?>" target="_blank">
								<i class="fab fa-twitter"></i>
							</a>
						</li>
						<?php endif; if($yt):?>
						<li>
							<a href="<?php echo $yt; ?>" target="_blank">
								<i class="fab fa-youtube"></i>
							</a>
						</li>
						<?php endif;?>
					</ul>
				</div>
			</div>
		</nav>
	</footer>
	
	<?php wp_footer(); ?>
	</body>
</html>
