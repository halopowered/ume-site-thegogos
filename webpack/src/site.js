import '@fancyapps/fancybox/dist/jquery.fancybox';
import 'slick-carousel/slick/slick.scss';

import $ from 'jquery';

import '@fancyapps/fancybox';
import 'slick-carousel';

$(document).ready(() => {
  // run after everything is loaded
  var interval = setInterval(function() {

    $('.menu-icon a').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('open');
      $('.mobile-menu, .l-main, .exit-off-canvas').toggleClass('open');
    })
    $('.exit-off-canvas').on('click', function(e) {
      e.preventDefault();
      if( $('.mobile-menu').hasClass('open') ) {
        $('.menu-icon a, .mobile-menu, .l-main, .exit-off-canvas').removeClass('open');
      }
    })

    if ( $('body').hasClass('page-template-template-videos-landing')) {
      $(".more-nodes-container .node--videos .node-overlay a").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600, 
        'speedOut'		:	200, 
        'overlayShow'	:	false
      });
    }

    $('.home section.l-banner .l-region--banner').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // fade: true,
      prevArrow: '<button class="slick-prev slick-arrow">Prev</button>',
      nextArrow: '<button class="slick-next slick-arrow">Next</button>',
    });

    $('.home .release-block .view-content').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // fade: true,
      prevArrow: '<button class="slick-prev slick-arrow">Prev</button>',
      nextArrow: '<button class="slick-next slick-arrow">Next</button>',
    });

    $('.photos-carousel, .page-template-template-bio .blocks-gallery-grid').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // fade: true,
      prevArrow: '<button class="slick-prev slick-arrow">Prev</button>',
      nextArrow: '<button class="slick-next slick-arrow">Next</button>',
    });

    // home splash videos
    var videoPlay = $('.home .videos-block .node--video .field--name-node-link');

    // take youtube ID and create an iframe with it
    videoPlay.on('click', function (event) {
      event.preventDefault();

      var playBtn = $(this);
      var youtubeID = playBtn.siblings('.field--name-field-youtube-id').text().replace(/ /g,'');
      var embedCode = "<div class='media-youtube-video' id='videoPlayer'><iframe src='https://www.youtube.com/embed/" + youtubeID + "?showinfo=0&autohide=1&rel=0&autoplay=1&mute=1' frameborder='0' allowfullscreen></iframe><button class='video-pause'>Pause</button></div>";

      if (!document.querySelector('.media-youtube-video')) {
        // Make sure there isnt a iframe before adding a new one
        playBtn.parent().addClass('hide-off-hover');
        var videoContainer = playBtn.parent().prev();
        playBtn.parent().parent().addClass('video-playing');

        videoContainer.append(embedCode);
      } else {
        $('.ds-region--background').removeClass('video-playing');
        $('.ds-region--foreground').removeClass('hide-off-hover');
        var videoContainer = playBtn.parent().prev();
        playBtn.parent().addClass('hide-off-hover');
        playBtn.parent().parent().addClass('video-playing');
        $('.media-youtube-video').remove();

        videoContainer.append(embedCode);
      }

      // Add 500ms timeout to make sure animation plays
      setTimeout(function () {
        videoContainer.addClass('video-playing');
      }, 500);
    })

    // internal video banner videos
    var videoPlay = $('.page-template-template-videos-landing section.l-banner .node--video-banner .field--name-node-link, .single-video section.l-banner .node--video-banner .field--name-node-link');

    // take youtube ID and create an iframe with it
    videoPlay.on('click', function (event) {
      event.preventDefault();

      var playBtn = $(this);
      var youtubeID = playBtn.siblings('.field--name-field-youtube-id').text().replace(/ /g,'');
      var embedCode = "<div class='media-youtube-video' id='videoPlayer'><iframe src='https://www.youtube.com/embed/" + youtubeID + "?showinfo=0&autohide=1&rel=0&autoplay=1&mute=1' frameborder='0' allowfullscreen></iframe><button class='video-pause'>Pause</button></div>";

      if (!document.querySelector('.media-youtube-video')) {
        // Make sure there isnt a iframe before adding a new one
        playBtn.parent().addClass('hide-off-hover');
        var videoContainer = playBtn.parent().prev();
        playBtn.parent().parent().addClass('video-playing');

        videoContainer.append(embedCode);
      } else {
        $('.ds-region--column-1').removeClass('video-playing');
        var videoContainer = playBtn.parent().prev();
        playBtn.parent().addClass('hide-off-hover');
        playBtn.parent().parent().addClass('video-playing');
        $('.media-youtube-video').remove();

        videoContainer.append(embedCode);
      }

      // Add 500ms timeout to make sure animation plays
      setTimeout(function () {
        videoContainer.addClass('video-playing');
      }, 500);
    })

     // remove video when changing slides
     $('.more-nodes-container .node--videos .node-overlay a').on('click', function() {
        $('.video-playing').find('.media-youtube-video').remove();
        $('.node--video-banner .ds-region--column-1, .node--video-banner .field--name-field-headline-image').removeClass('video-playing');
    });

 
    
    clearInterval(interval);
  }, 100);
  
});