# Music Stack

A development environment to build JS/SCSS wordpress projects.

This environment creates a proxy for a URL so it is possible to work on an external site with a [script tag pointing to a local development file](#site-preperation)

This environment [supports ES8](https://babeljs.io/docs/en/6.26.3/babel-preset-env)


- [Music Stack](#music-stack)
	- [Prerequisites](#prerequisites)
	- [Get started](#get-started)
		- [Site Preperation](#site-preperation)
		- [Development](#development)
	- [Build](#build)

## Prerequisites

Node JS v10.16.0

## Get started

Navigate to the directory and install dependencies:

```bash
npm install
```

### Site Preperation

Update theme folder name in `webpack.config.js`
<!-- The external site will be proxied via `webpack-dev-server` and the URL must be provided in `configuration.json` -->

Example configuration json:
```js
const outputPath = path.resolve(__dirname, '../wp-content/themes/FOLDER_NAME/dist')
```

```json
const files = [
	"../wp-content/themes/FOLDER_NAME/**/*.php"
]
```

The external site for development requires a script tag for the development environment to work

```html
<script src="http://localhost:8080/dist/bundle.js"></script>
```

This script tag supports [HMR](https://webpack.js.org/concepts/hot-module-replacement/) and includes the `css` output.

**Note:** The build will still output `js` and `css` files separately

### Development

To start development:

```bash
npm start
```

This will start the project on [http://0.0.0.0:8080/](http://0.0.0.0:8080/)

The webpack output will be served from [http://localhost:8080/dist/](http://localhost:8080/dist/)

## Build

The build outputs to the `dist` directory. This directory should have the `js`, `css`, and asset files.

To create a production build:

```bash
npm run build
```
