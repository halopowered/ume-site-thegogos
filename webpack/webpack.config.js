const path = require('path');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const chokidar = require( 'chokidar' );

let devtool, scssLoaders, plugins, styleLoaders;
const development = process.env.NODE_ENV !== 'production';

const outputPath = path.resolve(__dirname, '../wp-content/themes/gogos_gogos/dist');
let publicPath;

scssLoaders = [
  'css-loader',
  {
    loader: 'postcss-loader',
    options: {
      plugins: () => [autoprefixer()]
    }
  },
  {
    loader: 'sass-loader',
  },
];

if (development === true) {
  // Development
  devtool = 'inline-source-map';
  styleLoaders = [
    'style-loader',
    ...scssLoaders
  ];
  plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ];
  publicPath = 'http://0.0.0.0:8080/dist/';
} else {
  // Production  
  styleLoaders = [{
      loader: MiniCssExtractPlugin.loader,
    },
    ...scssLoaders
  ];
  plugins = [new MiniCssExtractPlugin({
    filename: 'style.css'
  })];
}

module.exports = {
  devtool,
  devServer: {
    contentBase: './www',
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    hot: true,
    port: 8080,
    host: '0.0.0.0',
    before(app, server) {
      const files = [
        "../wp-content/themes/gogos_gogos/**/*.php"
      ];

      chokidar
        .watch(files, {
          alwaysStat: true,
          atomic: false,
          followSymlinks: false,
          ignoreInitial: true,
          ignorePermissionErrors: true,
          persistent: true,
          usePolling: true
        })
        .on('all', () => {
          server.sockWrite(server.sockets, "content-changed");
        });
    },
  },
  entry: './src/index.js',
  output: {
    path: outputPath,
    filename: 'bundle.js',
    publicPath
  },
  mode: development ? 'development' : 'produdction',
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread'],
          },
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: styleLoaders,
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [{
          loader: 'file-loader'
        }],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }
    ],
  },
  plugins
};